# Procesador de series temporales (en desarrollo 👨‍🔧)

**Actualmente se encuentra en una versión no funcional por un refactor general del código.**

    Microservice de procesamiento de datos de series temporales a traves de captura IOT.

La idea es que el micro servicio reciba datos y los capture para generar alerta, acciones, transformaciones, etc. o simplemente almacenarlos en una base de datos.

El micro servicio se divide en 3 partes:

- **Captura**: se encarga de capturar los datos de distintos protocolos de comunicación, como por ejemplo HTTP, Archivos, MQTT, API, etc.

- **Procesamiento**: se encarga de procesar los datos capturados. Los datos pueden ser procesados de distintas maneras, como por ejemplo, generar alertas, acciones, transformaciones, etc. y estos serán funciones que se ejecutaran en un orden determinado. Por ejemplo, se puede generar una alerta cuando un valor supere un umbral y luego ejecutar una acción para enviar un mail.

- **Almacenamiento**: es la ultima parte del servicio, se encarga de almacenar los datos capturados y procesados. Los datos pueden ser almacenados en distintos tipos de bases de datos, como por ejemplo, MySQL, MongoDB, InfluxDB, etc.

