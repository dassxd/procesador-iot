import logging
from logging import Logger
from typing import Any

from processor.data_tree import reduce_tree, expand_tree, find_tags, find_tag, write_tag, remove_tag


class StandardLogger:
    """
    Clase para la implementación de logs estándar.
    Ademas de guardar los registros en un archivo, también los imprime en consola si el modo debug esta activado.
    Niveles de registro:
        - INFO: Información general
        - WARNING: Advertencias
        - DEBUG: Información de depuración
        - ERROR: Errores

    Args (Constructor):
        name (str): El parámetro de nombre es una cadena que representa el nombre del registrador. Se utiliza para identificar el registrador al registrar mensajes.
        file_path (str): El parámetro `file_path` es una cadena que representa la ruta del archivo donde se escribirán los mensajes de registro.
        debug (bool): El parámetro `debug` es un indicador booleano que determina si el registrador debe establecerse en el nivel de depuración o en el nivel de información. Si `debug` se establece en `True`, el registrador se establecerá en el nivel de depuración, lo que significa que registrará información más detallada. Si `debug` se establece en `. Defaults to False

    Args (Métodos):
        msg (str): El parámetro `msg` es una cadena que representa el mensaje que se va a registrar.
        exception_info (bool): solo para el método de error. El parámetro `exception_info` es un  indicador booleano que determina si se debe registrar información sobre la excepción que se está  produciendo. Si `exception_info` se establece en `True`, el registrador registrará información  sobre la excepción que se está produciendo. Si `exception_info` se establece en `False`, el  registrador no registrará información sobre la excepción que se está produciendo. Defaults to False.

    Example:
        >>> logger = StandardLogger(name="test", file_path="test.log", debug=True)
        >>> logger.info("This is a test")
        INFO: This is a test
        >>> logger.warning("This is a test")
        WARNING: This is a test
        >>> logger.debug("This is a test")
        DEBUG: This is a test
        >>> logger.error("This is a test")
        ERROR: This is a test
    """

    def __init__(self, name: str, file_path: str, debug: bool = False):
        logging.basicConfig(filename=file_path, level=logging.DEBUG if debug else logging.INFO,
                            format='%(asctime)s - %(name)s - %(process)d - %(levelname)s - %(message)s')
        self.logger: Logger = logging.getLogger(name)
        self.debugging: bool = debug

    def info(self, msg: str) -> None:
        self.logger.info(msg=msg)
        if self.debugging:
            print("INFO:", msg)

    def warning(self, msg: str) -> None:
        self.logger.warning(msg=msg)
        if self.debugging:
            print("WARNING:", msg)

    def debug(self, msg: str) -> None:
        self.logger.debug(msg=msg)
        if self.debugging:
            print("DEBUG:", msg)

    def error(self, msg: str, exception_info: bool = False) -> None:
        self.logger.error(msg=msg, exc_info=exception_info)
        if self.debugging:
            print("ERROR:", msg)


class StandardPayload():
    """
    Esta clase describe la estructura y la implementación de un payload estándar. Un payload estándar es un diccionario que contiene los datos de un activo o paquete en particular.

    Args (Constructor):
        data_tree (dict): El parámetro `data_tree` es un diccionario que representa una estructura de árbol. Cada clave en el diccionario representa un nodo en el árbol y el valor correspondiente representa los nodos secundarios de ese nodo. El árbol puede tener múltiples niveles de anidamiento.
        timestamps (list): El parámetro `timestamps` el elemento es una cadena que representa la marca de tiempo de los datos.
        id (dict): El parámetro `id` es un diccionario que contiene los indicadores del activo o paquete en cuestión.
        is_expanded (bool): El parámetro `is_expanded` es un indicador booleano que indica si los datos están en formato expandido o no.
        **data_additional (dict): El parámetro `**data_additional` es un diccionario que contiene datos adicionales que no están en el árbol de datos. Estos datos se pueden utilizar para almacenar información como el nombre del activo, el nombre del paquete, etc.

    Los objetos de esta clase tienen los siguientes métodos:
        - find_tags: El método `find_tags` devuelve un diccionario de pares clave-valor donde la clave contiene la cadena `find` .
        - find_tag: El método `find_tag` devuelve el primer valor que coincida con la cadena `find` dada en el diccionario.
        - write_tag: El método `write_tag` agrega la etiqueta y el valor al diccionario.
        - remove_tag: El método elimina una etiqueta especificada de un diccionario y devuelve el diccionario modificado.
    """

    def __init__(self, data_tree: dict, timestamps, id: dict, is_expanded: bool = True, **data_additional):
        self.timestamps = timestamps
        self.id = id
        self.data_tree = reduce_tree(
            data_tree) if is_expanded else data_tree
        self.data_additional = {}
        for key, value in data_additional.items():
            self.data_additional[key] = value
        self.data_additional = data_additional if not is_expanded else reduce_tree(
            data_additional)

    def payload(self, expand: bool = True) -> dict:
        """
        La función `payload` devuelve un diccionario en la estructura de datos estándar.

        Args:
            expand (bool): El parámetro "expandir" es un indicador booleano que determina si se expande o no
        el árbol de datos en la carga útil. Defaults to True

        Returns:
            Un diccionario que representa la carga útil en la estructura de datos estándar.
        """
        payload = {}
        payload['timestamps'] = self.timestamps
        payload['id'] = self.id
        payload['tree'] = expand_tree(
            self.data_tree) if expand else self.data_tree
        payload.update(expand_tree(self.data_additional)
                       if expand else self.data_additional)
        return payload

    def find_tags(self, find: str, in_tree: bool = True) -> dict:
        """
        La función `find_tags` busca una etiqueta específica dentro de un árbol de datos.

        Args:
            find (str): El parámetro `find` es una cadena que representa la etiqueta o estructura de etiqueta que desea buscar en el árbol de datos.
            in_tree (bool): El parámetro `in_tree` es un valor booleano que si `in_tree` se establece en `True`, la búsqueda se limitará únicamente al la ramificación de tree. Defaults to True

        Returns:
            El método `find_tags` devuelve un diccionario que contiene los pares clave-valor que coinciden con los criterios de búsqueda especificados por el parámetro `find`. Si no se encuentran coincidencias, devuelve "Ninguno".
        """
        return find_tags(self.data_tree if in_tree else self.data_additional, find, False)

    def find_tag(self, find: str, in_tree: bool = True) -> Any | None:
        """
        La función `find_tag` devuelve el primer valor que coincida con la cadena `find` dada en el diccionario.

        Args:
            find (str): El parámetro `find` es una cadena que representa la etiqueta que está buscando en los datos.
            in_tree (bool): El parámetro `in_tree` es un indicador booleano que indica si la búsqueda debe realizarse dentro de la clave 'tree' del diccionario `data`. Si `in_tree` se establece en `True`, la búsqueda se realizará dentro de la clave 'tree'. Si `in_tree` es. Defaults to True

        Returns:
            El valor que coincida con la etiqueta de búsqueda dada. Si no se encuentran coincidencias, devuelve Ninguno.
        """
        return find_tag(self.data_tree if in_tree else self.data_additional, find, False)

    def write_tag(self, tag_reduce: str, value, in_tree: bool = True) -> dict:
        """
        La función escribe una etiqueta con un valor reducido en un árbol de datos y, opcionalmente, actualiza la carga útil en su lugar.

        Args:
            tag_reduce (str): El parámetro `tag_reduce` es una cadena que representa la etiqueta o etiquetas que desea reducir o modificar en el árbol de datos.
            value: Es el valor que desea asignar a la etiqueta especificada.
            in_tree (bool): El parámetro `in_tree` es un indicador booleano que indica si la búsqueda debe realizarse dentro de la clave 'tree' del diccionario `data`. Si `in_tree` se establece en `True`, la búsqueda se realizará dentro de la clave 'tree'. Si `in_tree` es. Defaults to True

        Returns:
            los datos actualizados después de escribir la etiqueta.
        """
        data = write_tag(
            self.data_tree if in_tree else self.data_additional, tag_reduce, value, False)
        if in_tree:
            self.data_tree = data
        else:
            self.data_additional = data
        return data

    def remove_tag(self, tag_reduce: str, in_tree: bool = True) -> dict:
        """
        La función elimina una etiqueta especificada.

        Args:
            tag_reduced (str): El parámetro `tag_reduced` es una cadena que representa la etiqueta que desea eliminar.
            in_tree (bool): El parámetro `in_tree` es un indicador booleano que indica si la búsqueda debe realizarse dentro de la clave 'tree' del diccionario `data`. Si `in_tree` se establece en `True`, la búsqueda se realizará dentro de la clave 'tree'. Si `in_tree` es. Defaults to True

        Returns:
            El diccionario de datos modificado. Si la etiqueta especificada por `tag_reduced` se encuentra en el diccionario, se elimina y se devuelve el diccionario modificado. Si no se encuentra la etiqueta, se devuelve sin modificar.
        """
        data = remove_tag(
            self.data_tree if in_tree else self.data_additional, tag_reduce, False)
        if in_tree:
            self.data_tree = data if data != None else self.data_tree
        else:
            self.data_additional = data if data != None else self.data_additional
        return data
