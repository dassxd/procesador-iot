from json import loads
from datetime import datetime
from multiprocessing import current_process, Array, Queue, Process
from threading import Thread

from paho.mqtt import client as mqtt
from pytz import timezone

from processor.standard import StandardPayload


class ProcessorCore:

    """
    Clase principal del servicio de procesamiento de datos.

    ! NO PERMITE PROCESAR PAQUETES DE DATOS CON INFORMACIÓN VECTORIAL. ES DECIR, SOLO PERMITE PROCESAR UNA ESTAMPA DE TIEMPO POR PAQUETE DE DATOS.

    Args:
        flow (dict): Flujo de datos a procesar.
        run_mode (str, optional): Modo de ejecución del servicio. Puede ser "lineal" o "threads". Defaults to "lineal".
        number_of_threads (int, optional): Numero de hilos a usar, si es 0, es elegido por el servicio. solo disponible para run_mode = "threads". Defaults to 0.
        batch_size (int, optional): Tamaño de los batch de datos a guardar. Defaults to 0.
        batch_time (int, optional): Tiempo en segundos de espera entre batch de datos. Defaults to 5.

    Methods:
        model_data_shared: Genera los objetos de memoria compartida entre procesos.
        run: Ejecuta el servicio de procesamiento de datos.
    """

    def __init__(self, flow: dict, run_mode: str = "lineal", number_of_threads: int = 0, batch_size: int = 0, batch_time: int = 5):
        self.flow = flow
        self.run_mode = run_mode
        # Numero de hilos a usar, si es 0, es elegido por el servicio. solo disponible para run_mode = "threads".
        self.number_of_threads = number_of_threads
        self.batch_size = batch_size
        self.batch_time = batch_time
        self.queue_received, self.queue_processed, self.memory_shared = self.model_data_shared()

    @staticmethod
    def model_data_shared():
        queue_received = Queue()
        queue_processed = Queue()
        memory_shared = Array("i", (
            0,  # 0: Total de mensajes recibidos
            0,  # 1: Total de mensajes en cola
            0,  # 2: Total de mensajes procesados
            0,  # 3: Total de mensajes escritos a la base de datos
            0,  # 4: Total de errores
            0   # 5: Estado del proceso
        ))
        return queue_received, queue_processed, memory_shared

    def _flow_handler(self, step_flow: str, package: dict):
        if step_flow == "input":
            if self.enable_processing:
                self.queue_received.put(package)
            elif self.enable_output:
                self.queue_processed.put(package)
            else:
                pass  # No se carga el paquete a ninguna cola
        elif step_flow == "flow_processor":
            if self.enable_output:
                self.queue_processed.put(package)
            else:
                pass  # No se carga el paquete a ninguna cola

    def _get_asset_from_payload(self, payload: dict):
        """
        Identifica el asset del payload, para el correcto funcionamiento del core y el flujo de datos.

        Para identificar correctamente el asset se debe tener la siguiente estructura en el payload:
        `{"id": {"asset": "asset_name"}}`

        Y en el árbol de datos:
        `{"tree": {"tag": {"value": 0.0,"quality": 0}}}`
        """

        # Si timestamps es una lista, es un vector de datos
        is_vector = type(payload["timestamps"]) == list
        identifiers = payload.get("id", None)  # Identificadores del asset
        asset = None
        if type(identifiers) == list:  # Si es una lista, se toma el primer elemento
            # Si hay mas de un elemento, no se puede identificar el asset
            asset = identifiers[0] if len(identifiers) == 1 else None
        elif identifiers != None:  # Si no es una lista, se consulta el asset
            if not is_vector:  # Si no es un vector, se toma el asset directamente si existe
                asset = payload["id"].get("asset", None)
            else:  # Si es un vector, se toma el asset del primer elemento si existe
                asset = payload["id"].get("asset", None)[0]
        if asset == None:  # Si no se pudo identificar el asset, se toma el primer elemento del árbol de datos
            # Se toman los assets del árbol de datos, correspondiente al primer nivel de profundidad del árbol
            assets = list(payload["tree"].keys())
            # Si hay mas de un elemento, no se puede identificar el asset
            asset = assets[0] if len(assets) == 1 else None
        return identifiers, asset  # Se retorna el asset y los identificadores

    def _subscriber_mqtt(self, mqtt_client, topics: list):

        def on_message_mqtt(client, user_data, message):
            self.memory_shared[0] += 1
            try:
                json_data = loads(message.payload)

                # Busca el asset en el payload
                identifiers, asset = self._get_asset_from_payload(json_data)
                print("💢 {} - {}".format(asset, message.topic))
                # Si asset es None, no pudo ser identificado

                """
                ! FALTA AUN COLOCAR EL FILTRADO DE TAGS
                """

                payload = StandardPayload(
                    data_tree=json_data["tree"],
                    timestamps=json_data["timestamps"],
                    # Si no hay identificadores, se envía un diccionario vació.
                    id=json_data["id"] if identifiers != None else {},
                    is_expanded=True,
                )
                package = {
                    "asset": asset,
                    "payload": payload
                }
                self._flow_handler("input", package)
                self.memory_shared[1] += 1
            except Exception as e:
                self.memory_shared[4] += 1
                print("Exception on message:", e)
                print("==>", json_data)

        try:
            client = mqtt.Client(
                client_id="",
                clean_session=None,
                userdata=None,
                protocol=mqtt.MQTTv311,
                transport="tcp",
                reconnect_on_failure=True
            )
            if mqtt_client.tls != None:
                client.tls_set(
                    ca_certs=mqtt_client.tls["ca_certs"],
                    certfile=None,
                    keyfile=None,
                    cert_reqs=mqtt_client.tls["cert_reqs"],
                    tls_version=mqtt_client.tls["tls_version"],
                    ciphers=None
                )
                client.tls_insecure_set(mqtt_client.tls["insecure"])

            client.on_message = on_message_mqtt
            client.connect(
                host=mqtt_client.hostname,
                port=mqtt_client.port,
                keepalive=60,
            )
            print("🔌 Conectado a broker mqtt: {}:{}".format(
                mqtt_client.hostname, mqtt_client.port))
            client.subscribe(
                topic=[(topic, 0) for topic in topics],
            )
            client.loop_start()
            return client
        except Exception as e:
            print("Error core-mqtt:", e.args)

    def _subscriber_api(self):
        # ! AUN POR DESARROLLAR LA API PARA LA RECEPCIÓN DE DATOS
        pass

    def _input_handler(self, input_protocols_active: list = [], metadata_mqtt: dict = {}, metadata_api: dict = {}):
        if "mqtt" in input_protocols_active:
            clients_mqtt = []
            for hostname, metadata in metadata_mqtt.items():
                print(
                    "Cliente mqtt: {} - {}".format(hostname, metadata["topics"]))
                client_mqtt = self._subscriber_mqtt(
                    mqtt_client=metadata["client"],
                    topics=metadata["topics"],
                )
                if client_mqtt != None:
                    clients_mqtt.append(client_mqtt)
        if "api" in input_protocols_active:
            """
            ! DESARROLLAR LA API
            """
            self._subscriber_api()
            pass

    def _output_handler(self, output_protocols_active: list, metadata_influxdb: dict):
        if self.batch_size == 0:
            while True:
                if not self.queue_processed.empty():
                    try:
                        processed = self.queue_processed.get()
                        asset = processed["asset"]
                        if self.flow[asset]["active"] == False:
                            continue
                        for i, data_output in self.flow[asset]["output"].items():
                            if data_output["protocol"] == "influxdb":
                                self._write_influx(
                                    influx_client=data_output["client"],
                                    buffer_payloads=[processed]
                                )
                        self.memory_shared[3] += 1
                    except Exception as e:
                        print("Exception output_handler:", e)
                        self.memory_shared[4] += 1
        else:
            time_buffer = {}
            processed_buffer = {}
            while True:
                if not self.queue_processed.empty():
                    try:
                        processed = self.queue_processed.get()
                        asset = processed["asset"]
                        if self.flow[asset]["active"] == False:
                            continue
                        # Si no existe el buffer para el asset, se crea
                        if processed_buffer.get(asset, None) == None:
                            # Buffer de datos procesados
                            processed_buffer[asset] = []
                            # Buffer de tiempo
                            time_buffer[asset] = datetime.now()
                        processed_buffer[asset].append(processed)
                        # Si el buffer esta lleno o el tiempo de espera se cumple, se escribe en la base de datos
                        if self.batch_size == len(processed_buffer[asset]) or (datetime.now() - time_buffer[asset]).seconds >= self.batch_time:
                            for i, data_output in self.flow[asset]["output"].items():
                                # Protocolo de salida InfluxDB
                                if data_output["protocol"] == "influxdb":
                                    self._write_influx(
                                        influx_client=data_output["client"],
                                        buffer_payloads=processed_buffer[asset]
                                    )
                            self.memory_shared[3] += len(
                                processed_buffer[asset])
                            processed_buffer[asset] = []  # Se vacía el buffer
                            # Se reinicia el tiempo
                            time_buffer[asset] = datetime.now()
                    except Exception as e:
                        print("Exception output_handler:", e)
                        self.memory_shared[4] += len(processed_buffer[asset])

    def _write_influx(self, influx_client, buffer_payloads: list[dict]):
        client = influx_client.client()
        try:
            points = []
            for buffer in buffer_payloads:
                asset = buffer["asset"]
                payload = buffer["payload"]
                try:
                    time = datetime.fromtimestamp(
                        payload.timestamps, timezone("UTC"))
                    points.append({
                        "time": str(time),
                        "measurement": asset,
                        "fields": payload.data_tree
                    })
                except Exception as e:
                    print("Exception point create:", e)
            if self.enable_write_output:
                assert client.write_points(
                    points), "❌ Error al escribir los points"
                print(asset, "✅ Points escrito a influx", len(points))
            else:
                print(asset, "✅ Salidas desactivadas", len(points))
        except Exception as e:
            print("Exception _write_influx:", e.args)

    def run(
        self,
        debug: bool = False,
        enable_processing: bool = True,
        enable_output: bool = True,
        enable_write_output: bool = True
    ):
        self.debug = debug
        self.enable_processing = enable_processing
        self.enable_output = enable_output
        self.enable_write_output = enable_write_output
        """
        Ejecuta el servicio de procesamiento de datos
        """

        print("======================= MAPEO DE PROTOCOLOS =======================")
        # Barrido de protocolos activos
        input_protocols_active = []
        output_protocols_active = []

        # Metadatos de protocolos input
        metadata_mqtt = {}
        metadata_api = {}

        # Metadatos de protocolos output
        metadata_influxdb = {}

        for asset, data in self.flow.items():
            print("Asset: {} - {}".format(asset,
                  "Activo" if data["active"] else "Inactivo"))
            if data["active"] == False:
                continue

            # Protocolos de entrada
            for i, data_input in data["input"].items():
                if data_input["protocol"] not in input_protocols_active:
                    input_protocols_active.append(data_input["protocol"])

                # Extracción de metadatos para mqtt, se genera un cliente por cada hostname, este consulta los tópicos y filtra los tags.
                if data_input["protocol"] == "mqtt":
                    if (data_input["client"].hostname in metadata_mqtt.keys()) == False:
                        metadata_mqtt[data_input["client"].hostname] = {
                            "topics": [],
                            # Se guarda el cliente para usarlo en la conexión
                            "client": data_input["client"]
                        }
                    metadata_mqtt[data_input["client"].hostname]["topics"] += data_input["topics"]

            # Protocolos de salida
            if self.enable_output:
                for i, data_output in data["output"].items():
                    if data_output["protocol"] not in output_protocols_active:
                        output_protocols_active.append(data_output["protocol"])
                    if data_output["protocol"] == "influxdb":
                        client = data_output["client"].client()
                        try:
                            version = client.ping()
                            print(
                                "InfluxDB connection: {} - v {}".format(asset, version))
                        except Exception as e:
                            print("Exception InfluxDB:", e.args)

        print("Protocolos de entrada activos: {}".format(input_protocols_active))
        print("Protocolos de salida activos: {}".format(output_protocols_active))
        if not len(input_protocols_active) > 0:
            raise Exception(
                "❌ No se puede iniciar el core, no hay protocolos de entrada activos.")
        if self.enable_output:
            if not len(output_protocols_active) > 0:
                raise Exception(
                    "❌ No se puede iniciar el core, no hay protocolos de salida activos.")

        print("======================= INIT INPUTS =======================")
        # Proceso para manejar los protocolos de entrada de forma independiente al procesamiento de datos.
        # TODO: POR EL MOMENTO SOLO SE USA MQTT Y NO ES UN PROCESO QUE DEBA PERMANECER ACTIVO PERO SE DEJA COMO HILO POR SI SE NECESITA EN EL FUTURO.
        process_input = Thread(
            target=self._input_handler,
            args=(input_protocols_active, metadata_mqtt, metadata_api)
        )
        process_input.start()

        if self.enable_output:
            print(
                "======================= INIT OUTPUTS =======================")
            # Proceso para manejar los protocolos de salida de forma independiente al procesamiento de datos.
            process_output = Thread(
                target=self._output_handler,
                args=(output_protocols_active, metadata_influxdb)
            )
            process_output.start()

        print("======================= PROCESSING =======================")
        if not self.enable_processing:
            while True:
                pass
        if self.run_mode == "lineal":
            while True:
                try:
                    if not self.queue_received.empty():
                        received = self.queue_received.get()
                        asset = received["asset"]
                        payload = received["payload"]
                        print("🔨 Payload recibido - {}".format(asset))
                        assert self.flow.get(
                            asset, None) != None, "❌ Asset no encontrado en el flujo de datos."
                        for i, processor in self.flow[asset]["flow_processor"].items():
                            try:
                                if processor['active']:
                                    payload = processor['function'](
                                        payload, *processor['arguments'])
                            except Exception as e:
                                print("Exception flow_processor.",
                                      processor['function'].__name__, ":", e)
                        package = {
                            "asset": asset,
                            "payload": payload
                        }
                        self._flow_handler("flow_processor", package)
                except Exception as e:
                    print("Exception flow_processor:", e)
        elif self.run_mode == "threads":
            """
            ! AUN POR DESARROLLAR
            TODO: RECOMENDADO PARA ALTO TRAFICO DE DATOS Y GRANDES NÚMEROS DE ASSETS
            Procesado a traves de hilos
            Usa los hilos disponibles del sistema operativo para procesar los assets.
            Si number_of_threads = 0, usa: (hilos disponibles del sistema operativo / 2) + 1
            """
            pass
