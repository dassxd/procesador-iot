from platform import system
from subprocess import check_output
from os.path import basename
from os import system


def ping(host: str) -> bool:
    """
    La función `ping` toma un host como entrada y devuelve True si el host responde a una solicitud de ping y False en caso contrario.

    Args:
        host (str): El parámetro `host` es una cadena que representa la dirección IP o el nombre de host del host al que desea hacer ping.

    Returns:
        un valor booleano. Devuelve True si el comando ping tiene éxito (el código de respuesta es 0) y False en caso contrario.

    Example:
        >>> ping("www.google.com")
        True
    """
    response = system("ping -c 1 " + host)
    return True if response == 0 else False


def check_if_script_running(script_name: str) -> bool:
    """
    La función `check_if_script_running` verifica si un script con un nombre dado se está ejecutando actualmente en un sistema Linux.

    Args:
        script_name (str): El parámetro `script_name` es una cadena que representa el nombre del script que desea verificar si se está ejecutando.

    Returns:
        un valor booleano. Devuelve True si el script especificado se está ejecutando y False en caso contrario.
    """
    platform_os = system()
    if platform_os == "Linux":
        #! SOLO PARA LINUX
        command = check_output(
            "ps -aux | grep python3", shell=True).split(b'\n')
        result = [1 if basename(script_name) in i.decode(
            "utf-8") else 0 for i in command]
        return True if sum(result) > 2 else False

    return False
