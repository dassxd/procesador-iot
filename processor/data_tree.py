from typing import Any


def reduce_tree(tree: dict) -> dict:
    """
    La función `reduce_tree` toma un diccionario anidado como entrada y devuelve un diccionario aplanado donde las claves son las rutas a los valores en el diccionario original.
    Args:
        tree (dict): El parámetro `tree` es un diccionario que representa una estructura de árbol. Cada clave en el diccionario representa un nodo en el árbol y el valor correspondiente representa los nodos secundarios de ese nodo. El árbol puede tener múltiples niveles de anidamiento.
    Returns:
        La función `reduce_tree` devuelve un diccionario.
    Example:
        >>> tree = {
        ...     "a": {
        ...         "b": {
        ...             "c": 1,
        ...             "d": 2
        ...         },
        ...         "e": 3
        ...     }
        ... }
        >>> reduce_tree(tree)
        {'a.b.c': 1, 'a.b.d': 2, 'a.e': 3}
    """
    def _walk_json(_tree, _path=[]):
        try:
            for root, child in _tree.items():
                yield from _walk_json(child, _path + [root])
        except AttributeError:
            yield _path + [_tree]
    w = list(_walk_json(tree))
    result = {}
    for x in w:
        z = []
        for y in range(0, len(x)-1):
            z.append(x[y])
        result['.'.join(z)] = x[-1]
    return result


def expand_tree(data: dict) -> dict:
    """
    La función `expand_tree` toma un diccionario `datos` como entrada y devuelve un diccionario anidado que representa una estructura de árbol basada en las claves y valores en el diccionario de entrada.
    Args:
        data (dict): El parámetro `data` es un diccionario que representa una estructura de árbol. Cada clave en el diccionario representa una ruta en el árbol y el valor correspondiente representa el valor en esa ruta. Las claves están en el formato "a.b.c" donde "a", "b" y "c" son
    Returns:
        La función `expand_tree` devuelve un diccionario que representa una estructura de árbol expandida basada en los datos de entrada.
    Example:
        >>> data = {
        ...     "a.b.c": 1,
        ...     "a.b.d": 2,
        ...     "a.e": 3
        ... }
        >>> expand_tree(data)
        {'a': {'b': {'c': 1, 'd': 2}, 'e': 3}}
    """
    tree = {}
    for key, value in data.items():
        path = key.split(".")
        _tree = tree
        for i, step in enumerate(path):
            if step not in _tree:
                _tree[step] = {} if i < len(path) - 1 else value
            _tree = _tree[step]
    return tree


def find_tags(data: dict, find: str, is_expanded: bool = True) -> dict:
    """
    La función `find_tags` devuelve un diccionario de pares clave-valor donde la clave contiene la cadena `find` .
    Args:
        data (dict): Un diccionario que contiene los datos para buscar. Pueden ser los datos completos o solo la parte del 'árbol' de los datos, según el valor del parámetro 'in_tree'.
        find (str): El parámetro "buscar" es una cadena que representa la sub-cadena a buscar en las claves del diccionario de datos dado.
        is_expanded (bool): El parámetro "is_expanded" es un indicador booleano que determina si los datos deben expandirse o no. Si se establece en True, los datos se expandirán utilizando el método "expand_tree" de la clase  Si se establece en False, los datos no se expandirán. Defaults to True
    Returns:
        El método `find_tags` devuelve un diccionario que contiene los pares clave-valor que coinciden con los criterios de búsqueda especificados por el parámetro `find`. Si no se encuentran coincidencias, devuelve "Ninguno".
    """
    founds = {}
    data = reduce_tree(data) if is_expanded else data
    for i, v in data.items():
        if i.find(find) >= 0:
            founds[i] = v
    result = expand_tree(founds) if is_expanded else founds
    return result if len(result) > 0 else None


def find_tag(data: dict, find: str, is_expanded: bool = True) -> Any | None:
    """
    La función `find_tag` devuelve el primer valor que coincida con la cadena `find` dada en el diccionario.
    Args:
        data (dict): El parámetro "datos" es un diccionario que representa una estructura de árbol. Contiene listas y diccionarios anidados, donde cada diccionario representa un nodo en el árbol y cada lista representa una colección de nodos secundarios.
        find (str): El parámetro `find` es una cadena que representa la etiqueta que está buscando en los datos.
        is_expanded (bool): El parámetro `is_expanded` es un indicador booleano que indica si los datos están en formato expandido o no. Si `is_expanded` es `True`, significa que los datos están en un formato expandido y deben reducirse antes de buscar la etiqueta. Si `está_expandido. Defaults to True
    Returns:
        El valor que coincida con la etiqueta de búsqueda dada. Si no se encuentran coincidencias, devuelve Ninguno.
    """
    data = reduce_tree(data) if is_expanded else data
    for i, v in data.items():
        if i.find(find) >= 0:
            return v
    return None


def write_tag(data: dict, tag_reduced: str, value, is_expanded: bool = True) -> dict:
    """
    La función `write_tag` agrega la etiqueta y el valor al diccionario y expande el diccionario si se especifica.
    Args:
        data (dict): Un diccionario que representa el árbol de datos.
        tag_reduced (str): El parámetro tag_reduced es una cadena que representa la etiqueta reducida de los datos.
        value : El parámetro `valor` es el valor que se asignará a la etiqueta especificada en el diccionario de datos.
        is_expanded (bool): Un parámetro booleano que indica si el árbol de datos debe expandirse o reducirse. Si se establece en Verdadero, el árbol de datos se expandirá antes de agregar la etiqueta y el valor. Si se establece en False, el árbol de datos se reducirá antes de agregar la etiqueta y el valor. Defaults to True
    Returns:
        el diccionario de datos actualizado si pudo agregar la etiqueta y el valor.
    """
    data = reduce_tree(data) if is_expanded else data
    data.update({tag_reduced: value})
    return expand_tree(data) if is_expanded else data


def remove_tag(data: dict, tag_reduced: str, is_expanded: bool = True) -> dict | None:
    """
    La función elimina una etiqueta especificada de un diccionario y devuelve el diccionario modificado.
    Args:
      data (dict): Un diccionario que representa el árbol de datos.
      tag_reduced (str): El parámetro `tag_reduced` es una cadena que representa la etiqueta que desea eliminar del diccionario de datos.
      is_expanded (bool): El parámetro "is_expanded" es un indicador booleano que indica si los datos están en formato expandido o no. Si se establece en Verdadero, los datos se convertirán a un formato expandido utilizando la función "expand_tree" antes de devolver el resultado. Si se establece en Falso. Defaults to True
    Returns:
      el diccionario de datos modificado. Si la etiqueta especificada por `tag_reduced` se encuentra en el diccionario, se elimina y se devuelve el diccionario modificado. Si no se encuentra la etiqueta, se devuelve "Ninguno".
    """
    data = reduce_tree(data) if is_expanded else data
    result = (data.pop(tag_reduced, None))
    return (expand_tree(data) if is_expanded else data) if result != None else None


